- Tema Blue di Limosine (scuro) per Firefox:
https://addons.mozilla.org/it/firefox/addon/blue-theme-by-limosine/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search

- Tema ¡Light Blue di Miguel Reyes (chiaro) per Firefox:
https://addons.mozilla.org/it/firefox/addon/light-blue-theme-comments/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search

- Tabs di Firefox più basse:
https://leochavez.org/index.php/2023/03/06/make-firefoxs-tabs-smaller/

- Estensione Tabliss:
https://addons.mozilla.org/en-US/firefox/addon/tabliss/

- Impostazioni di sistema -> Schermata di accesso

NOTA: lo sfondo personalizzato per il login grafico è installato tramite install-res.sh

Sfondo: /usr/share/backgrounds/login-window/background_02_it.png
Tema GTK: BlueSky-Clean-Dark
Tema delle icone: Inverse-blue-dark
Puntatore del mouse: Quintom_Snow

La modifica del file slick-greeter.conf per aggiungere il tipo di carattere va fatta SOLO DOPO aver modificato le impostazioni della schermata di accesso (il file viene creato dopo aver modificato le impostazioni della stessa schermata di accesso):
$ sudo nano /etc/lightdm/slick-greeter.conf
font-name=Hurmit Nerd Font 10

- Installare il tema icone Sifr per LibreOffice:
$ sudo apt install libreoffice-style-sifr -y

- Installare il tema icone Papirus per LibreOffice:
Fonte: https://github.com/PapirusDevelopmentTeam/papirus-libreoffice-theme

Per installare le icone:
wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-libreoffice-theme/master/install-papirus-root.sh | sh

Per rimuovere le icone:
wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-libreoffice-theme/master/remove-papirus.sh | sh

