[ Modifiche per il menu di GRUB ]

- Attivare le icone per le voci "Advanced options" e "UEFI firmware settings":

Editare il file /etc/grub.d/10_linux e cambia la seguente linea:

echo "submenu '$(gettext_printf "Advanced options for %s" "${OS}" | grub_quote)' \$menuentry_id_option 'gnulinux-advanced-$boot_device_id' {"

come segue, aggiungendo la stringa "--class submenu":

echo "submenu '$(gettext_printf "Advanced options for %s" "${OS}" | grub_quote)' --class submenu \$menuentry_id_option 'gnulinux-advanced-$boot_device_id' {"

Editare il file /etc/grub.d/30_uefi-firmware e cambia la seguente linea:

menuentry '$LABEL' \$menuentry_id_option 'uefi-firmware' {

come segue, aggiungendo la stringa "--class efi":

menuentry '$LABEL' --class efi \$menuentry_id_option 'uefi-firmware' {

- Aggiungere le voci "Riavvia il Computer" e "Spegni il Computer" nel menu di Grub

Editare il file /etc/grub.d/40_custom e aggiungi:

menuentry 'Riavvia il Computer'  --class restart {
  reboot
}

menuentry 'Spegni il Computer'  --class shutdown {
  halt
}

- Editare il file /etc/default/grub e modificare o aggiungere le seguenti righe:

$ sudo nano /etc/default/grub

Modificare le righe:

GRUB_TIMEOUT_STYLE=hidden
GRUB_TIMEOUT=0
GRUB_GFXMODE=640x480

in:

GRUB_TIMEOUT_STYLE=menu
GRUB_TIMEOUT=10
GRUB_GFXMODE=1920x1080,auto

Aggiungere, alla fine del file:

GRUB_THEME="/boot/grub/themes/sugar-candy-bluesky/theme.txt"

Alla fine eseguire in un terminale il comando:
sudo update-grub

