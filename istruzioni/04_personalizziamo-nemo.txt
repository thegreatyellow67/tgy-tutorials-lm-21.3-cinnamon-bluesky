- Estensioni di Nemo
sudo apt install nemo-audio-tab nemo-compare nemo-dropbox nemo-emblems nemo-filename-repairer nemo-image-converter nemo-media-columns nemo-pastebin nemo-preview nemo-seahorse nemo-share nemo-terminal nemo-fileroller nemo-font-manager nemo-gtkhash nemo-nextcloud folder-color-switcher -y

- Azioni di Nemo personalizzate
Installare la dipendenza:
sudo apt install enscript -y

- Attivare altre azioni tramite Impostazioni di sistema -> Azioni
Verifica l'impronta sha256sum
Calcola l'impronta sha256sum
Calculate md5sum
Verify md5sum
Impostazioni di sistema
Riavvia Cinnamon

