- Disattivazione della schermata di benvenuto

- Aggiornamento del sistema

- Rispondere ai rapporti di sistema

- Settare la password di root
$ sudo -i
# passwd root

- Scaricare le risorse da GitLab con lo script install-res.sh

- Oppure scaricare le risorse in manuale con il comando:
git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-cinnamon-bluesky.git lm-21.3-cinnamon-bluesky

- Pacchetti vari da installare
$ sudo apt install fonts-powerline conky-all jq playerctl vlc ffmpeg dconf-editor tilix gpick gimp inkscape htop btop cava gpaste gir1.2-gpaste-1.0 rofi -y

- Spostarsi nella cartella ~/Scaricati/lm-21.3-cinnamon-bluesky
$ cd ~/Scaricati/lm-21.3-cinnamon-bluesky

- Caratteri usati
Hurmit Nerd Font: https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Hermit.zip

$ mkdir -p ~/.fonts
$ cp -r bluesky-fonts/* ~/.fonts
$ sudo cp -r bluesky-fonts/hurmit/ /usr/share/fonts/truetype/
$ fc-cache -frv
$ sudo fc-cache -frv

- Installare il tema GTK BlueSky
Fonte: https://github.com/i-mint/bluesky
       https://www.gnome-look.org/p/1271140

NOTA BENE: solitamente raccomando di aggiornare i temi GTK visitando periodicamente il sito del relativo sviluppatore, ma in questo caso vi consiglio di utilizzare quelli forniti da me, infatti ho modificato diverse proprietà del pannello di cinnamon e altre cose.

$ mkdir -p ~/.themes
$ cp -r bluesky-gtk-themes/* ~/.themes
$ sudo cp -r bluesky-gtk-themes/* /usr/share/themes
$ cp bluesky-gtk-themes/.config/gtk-3.0/gtk.css ~/.config/gtk-3.0/

- Installare le icone Inverse
Fonte: https://www.gnome-look.org/p/1344791
       https://github.com/yeyushengfan258/Inverse-icon-theme

$ mkdir -p ~/.icons
$ cp -r bluesky-icons/* ~/.icons
$ sudo cp -r bluesky-icons/* /usr/share/icons

- Installare i cursori Quintom
Fonte: https://www.gnome-look.org/p/1329799/

$ mkdir -p ~/.icons
$ cp -r bluesky-cursors/* ~/.icons
$ sudo cp -r bluesky-cursors/* /usr/share/icons

- Installare le icone per il menu
$ cp -r start-menu-icons/ ~/.icons

- Installare gli sfondi
$ cp -r bluesky-backgrounds/* ~/Immagini

- Installare le azioni per Nemo
$ cp -r nemo-actions/* ~/.local/share/nemo/actions

- Installare lo script di Conky
$ mkdir -p ~/.config/conky
$ cp -r conky-config/conky/* ~/.config/conky
$ cp conky-config/autostart/* ~/.config/autostart
$ cp conky-config/applications/* ~/.local/share/applications
$ sudo cp conky-config/script/launch-conky /usr/local/bin
$ sudo chmod 755 /usr/local/bin/launch-conky

- Installare la configurazione di cava
$ cp -r cava-config/.config/* ~/.config

- Installare la configurazione di glava
$ cp -r glava-config/.config/glava/ ~/.config
$ cp -r glava-config/.config/autostart/* ~/.config/autostart
$ cp -r glava-config/.local/share/applications/* ~/.local/share/applications
$ sudo cp -r glava-config/script/launch-glava /usr/local/bin
$ sudo chmod +x /usr/local/bin/launch-glava

- Installare lo sfondo per la finestra di accesso
$ sudo cp -r login-window/ /usr/share/backgrounds

- Installare macchina e relativa configurazione
$ sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
$ sudo chmod 755 /usr/local/bin/macchina
$ cp -r macchina/.config/* ~/.config

- Installare configurazione di neofetch
$ cp -r neofetch/ ~/.config

- Installare configurazione di rofi
$ cp -r rofi-config/.config/* ~/.config

- Installare il tema Sugar Candy BlueSky per Grub
$ sudo cp -r sugar-candy-bluesky/ /boot/grub/themes

- Sostituzione con utente corrente in alcuni files di configurazione
$ sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-command-launcher-menu-chiaro.json
$ sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-command-launcher-menu-scuro.json
$ sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-gestore-sessione.json
$ sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-menu.json
$ sed -i "s/tgy-tutorials/${USER}/g" dconf-settings/keybindings.conf

- Importazione impostazioni di dconf (scorciatoie tastiera)
$ dconf load /org/cinnamon/desktop/keybindings/ < dconf-settings/keybindings.conf
$ dconf load /org/gnome/terminal/ < dconf-settings/terminal.conf


