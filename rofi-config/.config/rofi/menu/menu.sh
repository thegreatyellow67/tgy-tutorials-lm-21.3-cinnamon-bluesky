#!/usr/bin/env bash

menu-help() {

clear

cat << _EOF_

 #-------------------------------------------------------------------#
 menu - Script per avviare il Menu di Rofi
 #-------------------------------------------------------------------#

 Comandi:

   menu --light = esegue lo script con tema chiaro

   menu --dark = esegue lo script con tema scuro

   menu --help = mostra questa pagina di aiuto

_EOF_

}

# Check for "--help" option:
if [ "$1" == "--help" ]; then
  menu-help
  exit
fi

# Menu light version
if [[ $1 == "--light" ]]; then
  theme='style-light'
fi

# Menu dark version
if [[ $1 == "--dark" ]]; then
  theme='style-dark'
fi

# Check for invalid arguments:
if  [ -z "$1"  ]; then
  clear
  echo
  echo " Errore script menu: argomento non valido! Prova 'launcher --help'" >&2
  echo
  exit 1
fi

# Current Theme
dir="$HOME/.config/rofi/menu"

## Esegui
rofi -show drun -theme ${dir}/${theme}.rasi

