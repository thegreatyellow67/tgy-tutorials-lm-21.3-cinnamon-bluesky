#!/usr/bin/env bash

powermenu-help() {

clear

cat << _EOF_

 #-------------------------------------------------------------------#
 powermenu - Script per avviare il PowerMenu di Rofi
 #-------------------------------------------------------------------#

 Comandi:

   powermenu --light = esegue lo script con tema chiaro

   powermenu --dark = esegue lo script con tema scuro

   powermenu --help = mostra questa pagina di aiuto

_EOF_

}

# Check for "--help" option:
if [ "$1" == "--help" ]; then
  powermenu-help
  exit
fi

# PowerMenu light version
if [[ $1 == "--light" ]]; then
  theme='style-light'
fi

# PowerMenu dark version
if [[ $1 == "--dark" ]]; then
  theme='style-dark'
fi

# Check for invalid arguments:
if  [ -z "$1"  ]; then
  clear
  echo
  echo " Errore script powermenu: argomento non valido! Prova 'powermenu --help'" >&2
  echo
  exit 1
fi

# Current Theme
dir="$HOME/.config/rofi/powermenu"

# CMDs
temp_uptime="`uptime -p | sed -e 's/up //g'`"
h_string=`echo ${temp_uptime} | cut -d " " -f 2 | sed 's/,//g'`
m_string=`echo ${temp_uptime} | cut -d " " -f 4`

if [ "${h_string}" == "hour" ]; then
  uptime=`echo ${temp_uptime} | sed 's/hour/ora/g'`
elif [ "${h_string}" == "hours" ]; then
  uptime=`echo ${temp_uptime} | sed 's/hours/ore/g'`
elif [ "${h_string}" == "minute" ] | [ "${h_string}" == "minutes" ]; then
  h_uptime="0 ore"
fi

if [ "${m_string}" == "minute" ] && [ "${h_uptime}" == "" ]; then
  uptime=`echo ${uptime} | sed 's/minute/minuto/g'`
elif [ "${m_string}" == "minutes" ] && [ "${h_uptime}" == "" ]; then
  uptime=`echo ${uptime} | sed 's/minutes/minuti/g'`
elif [ "${m_string}" == "" ]; then
  m_string=`echo ${temp_uptime} | cut -d " " -f 2 | sed 's/,//g'`

  if [ "${m_string}" == "minute" ];then
    m_uptime=`echo ${temp_uptime} | sed 's/minute/minuto/g'`
    uptime="${h_uptime} ${m_uptime}"
  elif [ "${m_string}" == "minutes" ];then
    m_uptime=`echo ${temp_uptime} | sed 's/minutes/minuti/g'`
    uptime="${h_uptime} ${m_uptime}"
  fi
fi

host=`hostname`

# Options
shutdown=''
reboot=''
lock=''
suspend=''
logout=''
yes=''
no=''

# Rofi CMD
rofi_cmd() {
	rofi -dmenu \
		-p "$host" \
		-mesg "Tempo di attività: ${uptime}" \
		-theme ${dir}/${theme}.rasi
}

# Confirmation CMD
confirm_cmd() {
	rofi -theme-str 'window {location: center; anchor: center; fullscreen: false; width: 350px;}' \
		-theme-str 'mainbox {children: [ "message", "listview" ];}' \
		-theme-str 'listview {columns: 2; lines: 1;}' \
		-theme-str 'element-text {horizontal-align: 0.5;}' \
		-theme-str 'textbox {horizontal-align: 0.5;}' \
		-dmenu \
		-p 'Confirmation' \
		-mesg 'Sei sicuro?' \
		-theme ${dir}/${theme}.rasi
}

# Ask for confirmation
confirm_exit() {
	echo -e "$yes\n$no" | confirm_cmd
}

# Pass variables to rofi dmenu
run_rofi() {
	echo -e "$lock\n$suspend\n$logout\n$reboot\n$shutdown" | rofi_cmd
}

# Execute Command
run_cmd() {
	selected="$(confirm_exit)"
	if [[ "$selected" == "$yes" ]]; then
		if [[ $1 == '--shutdown' ]]; then
			systemctl poweroff
		elif [[ $1 == '--reboot' ]]; then
			systemctl reboot
		elif [[ $1 == '--suspend' ]]; then
			mpc -q pause
			amixer set Master mute
			systemctl suspend
		elif [[ $1 == '--logout' ]]; then
			if [[ "$DESKTOP_SESSION" == 'openbox' ]]; then
				openbox --exit
			elif [[ "$DESKTOP_SESSION" == 'bspwm' ]]; then
				bspc quit
			elif [[ "$DESKTOP_SESSION" == 'i3' ]]; then
				i3-msg exit
			elif [[ "$DESKTOP_SESSION" == 'plasma' ]]; then
				qdbus org.kde.ksmserver /KSMServer logout 0 0 0
			elif [[ "$DESKTOP_SESSION" == 'cinnamon' ]]; then
				cinnamon-session-quit --logout --force
			fi
		fi
	else
		exit 0
	fi
}

# Actions
chosen="$(run_rofi)"
case ${chosen} in
    $shutdown)
		run_cmd --shutdown
        ;;
    $reboot)
		run_cmd --reboot
        ;;
    $lock)
		if [[ -x '/usr/bin/betterlockscreen' ]]; then
			betterlockscreen -l
		elif [[ -x '/usr/bin/i3lock' ]]; then
			i3lock
        elif [[ -x '/usr/bin/cinnamon-screensaver-command' ]]; then
            cinnamon-screensaver-command --lock
		fi
        ;;
    $suspend)
		run_cmd --suspend
        ;;
    $logout)
		run_cmd --logout
        ;;
esac
