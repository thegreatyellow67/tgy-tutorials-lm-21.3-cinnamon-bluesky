#!/bin/bash

clear

THEME="bluesky"
THEME_FOLDER="lm-21.3-cinnamon-${THEME}"
BASE_PATH="${HOME}/Scaricati/"

echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
echo ""
echo "  ██████╗ ██╗     ██╗   ██╗███████╗███████╗██╗  ██╗██╗   ██╗"
echo "  ██╔══██╗██║     ██║   ██║██╔════╝██╔════╝██║ ██╔╝╚██╗ ██╔╝"
echo "  ██████╔╝██║     ██║   ██║█████╗  ███████╗█████╔╝  ╚████╔╝"
echo "  ██╔══██╗██║     ██║   ██║██╔══╝  ╚════██║██╔═██╗   ╚██╔╝"
echo "  ██████╔╝███████╗╚██████╔╝███████╗███████║██║  ██╗   ██║"
echo "  ╚═════╝ ╚══════╝ ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝   ╚═╝"
echo ""
echo "  ===================================================================="
echo "  Script per sistemare eventuali problemi di permessi di file e"
echo "  sottocartelle per la cartella ${THEME_FOLDER}"
echo "  Scritto da TGY-TUTORIALS il 19/03/2024"
echo "  ===================================================================="

function goto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

echo ""
echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
    s|S)
        goto main
        ;;
    n|N)
        echo "  Termino lo script...a presto!"
        exit 1
        ;;
    *)
        echo "  Tasto non valido. Per favore premi 's' o 'n'."
        sleep 5
        exit 1
        ;;
esac

main:

if [ -d ${BASE_PATH} ];then
  cd ${BASE_PATH}

  if [ -d ${THEME_FOLDER} ];then
    find ${THEME_FOLDER} -type d -print0 | xargs -0 chmod -v 755
    find ${THEME_FOLDER} -type f -print0 | xargs -0 chmod -v 644
    chmod 755 ${THEME_FOLDER}/*.sh
    chmod 755 ${THEME_FOLDER}/conky-config/conky/Shaula-Dark/start.sh
    chmod 755 ${THEME_FOLDER}/conky-config/conky/Shaula-Dark/scripts/*
    chmod 755 ${THEME_FOLDER}/conky-config/conky/Shaula-Light/start.sh
    chmod 755 ${THEME_FOLDER}/conky-config/conky/Shaula-Light/scripts/*
    chmod 755 ${THEME_FOLDER}/conky-config/applications/Conky-Shaula-Light-Startup.desktop
    chmod 755 ${THEME_FOLDER}/conky-config/applications/Conky-Shaula-Dark-Startup.desktop
    chmod 755 ${THEME_FOLDER}/conky-config/autostart/Conky-Shaula-Dark-Startup.desktop
    chmod 755 ${THEME_FOLDER}/conky-config/script/launch-conky
    chmod 755 ${THEME_FOLDER}/glava-config/.config/autostart/Glava-Audio-Visualizer-Bars-Mode.desktop
    chmod 755 ${THEME_FOLDER}/glava-config/.local/share/applications/Glava-Audio-Visualizer-Bars-Mode.desktop
    chmod 755 ${THEME_FOLDER}/glava-config/script/launch-glava
    chmod 755 ${THEME_FOLDER}/macchina/macchina-linux-x86_64
    chmod 755 ${THEME_FOLDER}/rofi-config/.config/rofi/menu/menu.sh
    chmod 755 ${THEME_FOLDER}/rofi-config/.config/rofi/powermenu/powermenu.sh
  else
    echo ""
    echo "  La cartella ${THEME_FOLDER} non esiste!"
    echo ""
    exit 1
  fi
else
  echo ""
  echo "  La cartella ${BASE_PATH} non esiste!"
  echo ""
  exit 1
fi
