#!/bin/bash

DAYNAME=`date +"%^A"`

case ${DAYNAME} in

  LUNEDì)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 200x65 -p 2,63}'
    ;;

  MARTEDì)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 250x65 -p 2,63}'
    ;;

  MERCOLEDì)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 310x65 -p 2,63}'
    ;;

  GIOVEDì)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 220x65 -p 2,63}'
    ;;

  VENERDì)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 240x65 -p 2,63}'
    ;;

  SABATO)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 235x65 -p 2,63}'
    ;;

  DOMENICA)
    echo '${image ~/.config/conky/Shaula-Light/res/bg.png -s 290x65 -p 2,63}'
    ;;

  *)
    echo " Nome del giorno non definito o errore generico! Chiudo lo script..."
    exit 1
    ;;
esac

