#!/bin/bash

PCTL=$(playerctl status)

if [[ ${PCTL} == "" ]]; then
  echo "Artista (non disponibile)"
  exit 0
elif [[ ${PCTL} == "Stopped" ]]; then
  echo "Artista (non disponibile)"
  exit 0
else
  artist=`playerctl metadata xesam:artist`
  num_chars=`echo -n "$artist" | wc -c`

  if [[ ${num_chars} == 0 ]]; then
    echo "Artista (non disponibile)"
  else
    if [[ ${num_chars} -ge 30 ]]; then
      artist=`playerctl metadata xesam:title | cut -c1-29`
      echo ${artist}"..."
    else
      echo ${artist}
    fi
  fi
fi

exit
