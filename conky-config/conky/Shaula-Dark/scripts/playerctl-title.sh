#!/bin/bash

PCTL=$(playerctl status)

if [[ ${PCTL} == "" ]]; then
  echo "Titolo (non disponibile)"
  exit 0
elif [[ ${PCTL} == "Stopped" ]]; then
  echo "Titolo (non disponibile)"
  exit 0
else
  title=`playerctl metadata xesam:title`
  num_chars=`echo -n "$title" | wc -c`

  if [[ ${num_chars} -ge 30 ]]; then
    title=`playerctl metadata xesam:title | cut -c1-29`
    echo ${title}"..."
  else
    echo ${title}
  fi
fi

exit
