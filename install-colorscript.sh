#!/bin/bash

clear

echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
echo ""                                                                     
echo "  ██████╗ ██╗     ██╗   ██╗███████╗███████╗██╗  ██╗██╗   ██╗"
echo "  ██╔══██╗██║     ██║   ██║██╔════╝██╔════╝██║ ██╔╝╚██╗ ██╔╝"
echo "  ██████╔╝██║     ██║   ██║█████╗  ███████╗█████╔╝  ╚████╔╝"
echo "  ██╔══██╗██║     ██║   ██║██╔══╝  ╚════██║██╔═██╗   ╚██╔╝"
echo "  ██████╔╝███████╗╚██████╔╝███████╗███████║██║  ██╗   ██║"
echo "  ╚═════╝ ╚══════╝ ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝   ╚═╝"
echo ""
echo "  ===================================================================="
echo "  Script per installare colorscript"
echo "  https://gitlab.com/dwt1/shell-color-scripts"
echo "  di Derek Taylor aka Distrotube"
echo ""
echo "  Per mostrare la lista dei temi usa il comando:"
echo ""
echo "  colorscript -a"
echo ""
echo "  Per visualizzare un tema usa il comando:"
echo ""
echo "  colorscript -e [ nome_tema ]"
echo ""
echo "  Puoi aggiungere il comando colorscript -e [ nome_tema ]"
echo "  nel file ~/.bashrc per mostrare il tema scelto ad ogni"
echo "  apertura di un terminale"
echo ""
echo "  Scritto da TGY-TUTORIALS il 19/03/2024"
echo "  ===================================================================="

function goto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

echo ""
echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
    s|S)
        goto main
        ;;
    n|N)
        echo "  Termino lo script...a presto!"
        exit 1
        ;;
    *)
        echo "  Tasto non valido. Per favore premi 's' o 'n'."
        sleep 5
        exit 1
        ;;
esac

main:

BASE_PATH="${HOME}/Scaricati/"

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

echo ""
echo "  Sto per scaricare i sorgenti da GitLab, un pò di pazienza..."
echo ""

#
# Installa git se mancante
#
if ! location="$(type -p "git")" || [ -z "git" ]; then
  echo "  Installo git per far funzionare questo script..."
  sudo apt install -y git &> /dev/null
fi

git clone https://gitlab.com/dwt1/shell-color-scripts.git
cd shell-color-scripts
sudo make install
